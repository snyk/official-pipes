import unittest
from unittest.mock import patch, Mock

import yaml

from official_build.validators import BitbucketApiService, DockerHubAPIService
from official_build.validators import (
    Failures, RuleContext, PipeManifestFileValidator,
    PipeLatestVersionValidator, PipeGitTagValidator, PipeReadmeValidator,
    PipeYmlFragmentValidator, PipeDockerImageSizeValidator,
    PipeRemoteYamlValidator, PipeFinalYamlValidator
)
from test.fixtures import MANIFEST_YML, PIPE_YML, MANIFEST_YML_CUSTOM, YAML_FRAGMENT


MANIFEST_YML_FAILED_NO_VERISION = """
repositoryPath: 'atlassian/bitbucket-trigger-pipeline'
"""


class PipeManifestFileValidatorTest(unittest.TestCase):
    def setUp(self):
        self.empty_failures = Failures(criticals=[], warnings=[], skipped=[])

    def test_validate_no_failures(self):
        context = RuleContext(
            infile='pipe.yml',
            local_yml=yaml.safe_load(MANIFEST_YML),
            remote_yml={},
            final_yml={}
        )
        validator = PipeManifestFileValidator(context)
        assert validator.validate() == self.empty_failures

    def test_validate_with_failures(self):
        context = RuleContext(
            infile='pipe.yml',
            local_yml=yaml.safe_load(MANIFEST_YML_FAILED_NO_VERISION),
            remote_yml={},
            final_yml={}
        )
        validator = PipeManifestFileValidator(context)
        assert validator.validate() == Failures(criticals=["File pipe.yml not valid: {'version': ['required field']}"], warnings=[], skipped=[])


class PipeLatestVersionValidatorTest(unittest.TestCase):
    def setUp(self):
        self.empty_failures = Failures(criticals=[], warnings=[], skipped=[])

    @patch.object(BitbucketApiService, 'get_latest_tag', Mock(return_value='1.0.0'))
    def test_validate_no_failures(self):
        context = RuleContext(
            infile='pipe.yml',
            local_yml={},
            remote_yml={},
            final_yml=yaml.safe_load(MANIFEST_YML)
        )
        validator = PipeLatestVersionValidator(context)
        assert validator.validate() == self.empty_failures

    @patch.object(BitbucketApiService, 'get_latest_tag', Mock(return_value='1.0.0'))
    def test_validate_with_failures(self):
        context = RuleContext(
            infile='pipe.yml',
            local_yml={},
            remote_yml={},
            final_yml=yaml.safe_load(MANIFEST_YML_CUSTOM)
        )
        validator = PipeLatestVersionValidator(context)
        assert validator.validate() == Failures(
            criticals=[],
            warnings=[
                '1.0.0 version available. Current version is 0.1.0'
            ],
            skipped=[]
        )


class PipeGitTagValidatorTest(unittest.TestCase):
    def setUp(self):
        self.empty_failures = Failures(criticals=[], warnings=[], skipped=[])

    @patch.object(BitbucketApiService, 'get_latest_tag', Mock(return_value='1.0.0'))
    def test_validate_no_failures(self):
        context = RuleContext(
            infile='pipe.yml',
            local_yml={},
            remote_yml={},
            final_yml=yaml.safe_load(MANIFEST_YML)
        )
        validator = PipeGitTagValidator(context)
        assert validator.validate() == self.empty_failures

    @patch.object(BitbucketApiService, 'get_latest_tag', Mock(return_value=None))
    def test_validate_with_failures(self):
        context = RuleContext(
            infile='pipe.yml',
            local_yml={},
            remote_yml={},
            final_yml=yaml.safe_load(MANIFEST_YML_CUSTOM)
        )
        validator = PipeGitTagValidator(context)
        assert validator.validate() == Failures(
            criticals=[
                "Repository does not contain any tag. Please, release a version first."
            ],
            warnings=[],
            skipped=[]
        )


class PipeReadmeValidatorTest(unittest.TestCase):
    def setUp(self):
        self.empty_failures = Failures(criticals=[], warnings=[], skipped=[])

    @patch.object(BitbucketApiService, 'readme_exists', Mock(return_value=True))
    def test_validate_no_failures(self):
        context = RuleContext(
            infile='pipe.yml',
            local_yml={},
            remote_yml={},
            final_yml=yaml.safe_load(MANIFEST_YML)
        )
        validator = PipeReadmeValidator(context)
        assert validator.validate() == self.empty_failures

    @patch.object(BitbucketApiService, 'readme_exists', Mock(return_value=False))
    def test_validate_with_failures(self):
        context = RuleContext(
            infile='pipe.yml',
            local_yml={},
            remote_yml={},
            final_yml=yaml.safe_load(MANIFEST_YML_CUSTOM)
        )
        validator = PipeReadmeValidator(context)
        assert validator.validate() == Failures(
            criticals=[
                f"{context.infile} repository not valid: README.md not found"
            ],
            warnings=[],
            skipped=[]
        )


class PipeYmlFragmentValidatorTest(unittest.TestCase):
    NOT_VALID_YAML = "..."
    EMPTY_YAML = ""

    def setUp(self):
        self.empty_failures = Failures(criticals=[], warnings=[], skipped=[])
        self.yaml = {
            'name': 'Bitbucket trigger pipeline',
            'description': 'Trigger a pipeline in a Bitbucket repository.',
            'category': 'Workflow automation',
            'repositoryPath': 'atlassian/bitbucket-trigger-pipeline',
            'version': '1.0.0',
            'maintainer': {'name': 'Atlassian', 'website': 'https://www.atlassian.com/'},
            'vendor': {'name': 'Atlassian', 'website': 'https://www.atlassian.com/'},
            'logo': 'https://bytebucket.org/ravatar',
            'yml': YAML_FRAGMENT,
            'tags': ['pipelines', 'pipes', 'build', 'bitbucket'],
            'timestamp': "\"b'Tue, 09 Jun 2020 10:56:00 +0000\\n'\"",
            'created_at': '2020-06-09T10:56:00+00:00',
        }

    @patch.object(BitbucketApiService, 'get_yml_definition', Mock(return_value=YAML_FRAGMENT))
    def test_validate_no_failures(self):
        context = RuleContext(
            infile='pipe.yml',
            local_yml={},
            remote_yml={},
            final_yml=self.yaml
        )
        validator = PipeYmlFragmentValidator(context)
        assert validator.validate() == self.empty_failures

    @patch.object(BitbucketApiService, 'get_yml_definition', Mock(return_value=None))
    def test_validate_with_failures_yaml_not_equal(self):
        context = RuleContext(
            infile='pipe.yml',
            local_yml={},
            remote_yml={},
            final_yml=self.yaml
        )
        validator = PipeYmlFragmentValidator(context)
        assert validator.validate() == Failures(
            criticals=[
                f"{context.infile} yml field not equal"
            ],
            warnings=[],
            skipped=[]
        )

    @patch.object(BitbucketApiService, 'get_yml_definition', Mock(return_value=EMPTY_YAML))
    def test_validate_with_failures_yml_doesnt_exist(self):
        final_yaml_none_yaml = {k: v for k, v in self.yaml.items()}
        final_yaml_none_yaml['yml'] = self.EMPTY_YAML
        context = RuleContext(
            infile='pipe.yml',
            local_yml={},
            remote_yml={},
            final_yml=final_yaml_none_yaml
        )
        validator = PipeYmlFragmentValidator(context)
        assert validator.validate() == Failures(
            criticals=[
                f"{context.infile} yml definition in README.md does not exist"
            ],
            warnings=[],
            skipped=[]
        )

    @patch.object(BitbucketApiService, 'get_yml_definition', Mock(return_value=NOT_VALID_YAML))
    def test_validate_with_failures_yaml_non_valid(self):
        final_yaml_non_valid_yaml = {k: v for k, v in self.yaml.items()}
        final_yaml_non_valid_yaml['yml'] = self.NOT_VALID_YAML
        context = RuleContext(
            infile='pipe.yml',
            local_yml={},
            remote_yml={},
            final_yml=final_yaml_non_valid_yaml
        )
        validator = PipeYmlFragmentValidator(context)
        assert validator.validate() == Failures(
            criticals=[
                f"{context.infile} yml definition in README.md is not valid"
            ],
            warnings=[],
            skipped=[]
        )


class PipeDockerImageSizeValidatorTest(unittest.TestCase):
    def setUp(self):
        self.empty_failures = Failures(criticals=[], warnings=[], skipped=[])
        self.yaml = yaml.safe_load(PIPE_YML)

    @patch.object(DockerHubAPIService, 'get_size', Mock(return_value=int(1234232)))
    def test_validate_no_failures(self):
        context = RuleContext(
            infile='pipe.yml',
            local_yml={},
            remote_yml=self.yaml,
            final_yml={},
        )
        validator = PipeDockerImageSizeValidator(context)
        assert validator.validate() == self.empty_failures

    @patch.object(DockerHubAPIService, 'get_size', Mock(return_value=int(99999999999999999)))
    def test_validate_with_failures(self):
        context = RuleContext(
            infile='pipe.yml',
            local_yml={},
            remote_yml=self.yaml,
            final_yml={},
        )
        validator = PipeDockerImageSizeValidator(context)
        assert validator.validate() == Failures(
            criticals=[
                f"Docker image '{context.remote_yml['image']}' is larger than 1GB."
            ],
            warnings=[],
            skipped=[]
        )


class PipeRemoteYamlValidatorTest(unittest.TestCase):
    def setUp(self):
        self.empty_failures = Failures(criticals=[], warnings=[], skipped=[])
        self.yaml = yaml.safe_load(PIPE_YML)

    def test_validate_no_failures(self):
        context = RuleContext(
            infile='pipe.yml',
            local_yml={},
            remote_yml=self.yaml,
            final_yml={},
        )
        validator = PipeRemoteYamlValidator(context)
        assert validator.validate() == self.empty_failures

    def test_validate_with_failures_no_image(self):
        remote_yaml_non_valid = {k: v for k, v in self.yaml.items() if k != 'image'}
        context = RuleContext(
            infile='pipe.yml',
            local_yml={},
            remote_yml=remote_yaml_non_valid,
            final_yml={},
        )
        validator = PipeRemoteYamlValidator(context)
        assert validator.validate() == Failures(
            criticals=[
                "pipe.yml repository not valid: pipe.yml not valid: {'image': ['required field']}"
            ],
            warnings=[],
            skipped=[]
        )


class PipeFinalYamlValidatorTest(unittest.TestCase):
    def setUp(self):
        self.empty_failures = Failures(criticals=[], warnings=[], skipped=[])
        self.yaml = {
            'name': 'Bitbucket trigger pipeline',
            'description': 'Trigger a pipeline in a Bitbucket repository.',
            'category': 'Workflow automation',
            'repositoryPath': 'atlassian/bitbucket-trigger-pipeline',
            'version': '1.0.0',
            'maintainer': {'name': 'Atlassian', 'website': 'https://www.atlassian.com/'},
            'vendor': {'name': 'Atlassian', 'website': 'https://www.atlassian.com/'},
            'logo': 'https://bytebucket.org/ravatar',
            'yml': YAML_FRAGMENT,
            'tags': ['pipelines', 'pipes', 'build', 'bitbucket'],
            'timestamp': "\"b'Tue, 09 Jun 2020 10:56:00 +0000\\n'\"",
            'created_at': '2020-06-09T10:56:00+00:00',
        }

    def test_validate_no_failures(self):
        context = RuleContext(
            infile='pipe.yml',
            local_yml={},
            remote_yml={},
            final_yml=self.yaml,
        )
        validator = PipeFinalYamlValidator(context)
        assert validator.validate() == self.empty_failures

    def test_validate_with_failures_no_version(self):
        final_yaml_non_valid = {k: v for k, v in self.yaml.items() if k != 'version'}
        context = RuleContext(
            infile='pipe.yml',
            local_yml={},
            remote_yml={},
            final_yml=final_yaml_non_valid,
        )
        validator = PipeFinalYamlValidator(context)
        assert validator.validate() == Failures(
            criticals=[
                "pipe.yml final metadata not valid: {'version': ['required field']}"
            ],
            warnings=[],
            skipped=[]
        )
