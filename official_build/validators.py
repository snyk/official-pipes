'''
Validators
'''
from abc import ABCMeta, abstractmethod
import re
from typing import List, Optional, Tuple
from dataclasses import dataclass

import yaml
import docker
import cerberus

from official_build.constants import (
    WHITELIST_PIPES, REMOTE_METADATA_SCHEMA,
    LOCAL_METADATA_SCHEMA, FINAL_METADATA_SCHEMA
)
from official_build.api.bitbucket.base import BitbucketApiService
from official_build.api.docker.base import DockerHubAPIService
from official_build.utils import get_latest_git_tag


@dataclass
class RuleContext:
    infile: str
    local_yml: dict
    remote_yml: Optional[dict]
    final_yml: dict


@dataclass
class Failures:
    criticals: List[str]
    warnings: List[str]
    skipped: List[str]

    def extend(self, other):
        if other.criticals:
            self.criticals.extend(other.criticals)
        if other.warnings:
            self.warnings.extend(other.warnings)
        if other.skipped:
            self.skipped.extend(other.skipped)


class PipeMetadataValidator(metaclass=ABCMeta):
    def __init__(self, context: RuleContext):
        self.failures = Failures(criticals=[], warnings=[], skipped=[])
        self.context = context

    @abstractmethod
    def validate(self):
        pass


class PipeManifestFileValidator(PipeMetadataValidator):
    LOCAL_METADATA = cerberus.Validator(LOCAL_METADATA_SCHEMA)

    def validate(self):
        if not self.LOCAL_METADATA.validate(self.context.local_yml):
            msg = f"File {self.context.infile} not valid: {str(self.LOCAL_METADATA.errors)}"
            self.failures.criticals.append(msg)
        return self.failures


class PipeLatestVersionValidator(PipeMetadataValidator):

    def validate(self):
        yml = self.context.final_yml
        version = yml['version']
        latest_tag = get_latest_git_tag(yml['repositoryPath'])
        if latest_tag != version:
            self.failures.warnings.append(f"{latest_tag} version available. Current version is {version}")
        return self.failures


class PipeGitTagValidator(PipeMetadataValidator):

    def validate(self):
        yml = self.context.final_yml
        latest_tag = get_latest_git_tag(yml['repositoryPath'])
        if latest_tag is None:
            self.failures.criticals.append("Repository does not contain any tag. Please, release a version first.")
        return self.failures


class PipeReadmeValidator(PipeMetadataValidator):

    def validate(self):
        bitbucket_api_service = BitbucketApiService()
        yml = self.context.final_yml
        version = yml['version']
        repository_path = yml['repositoryPath']
        # Fetch the README.md
        if not bitbucket_api_service.readme_exists(repository_path, version):
            self.failures.criticals.append(f"{self.context.infile} repository not valid: README.md not found")
        return self.failures


class PipeYmlFragmentValidator(PipeMetadataValidator):

    def validate(self):
        bitbucket_api_service = BitbucketApiService()
        yml = self.context.final_yml
        infile = self.context.infile
        version = yml['version']
        repository_path = yml['repositoryPath']
        yml_definition = bitbucket_api_service.get_yml_definition(repository_path, version)

        if 'yml' in yml and yml_definition != yml['yml']:
            # TODO: check this until we make sure all code snippets are similar
            self.failures.criticals.append(f"{infile} yml field not equal")

        if not yml['yml']:
            self.failures.criticals.append(f"{infile} yml definition in README.md does not exist")
        try:
            yaml.safe_load(yml['yml'])
        except yaml.YAMLError:
            self.failures.criticals.append(f"{infile} yml definition in README.md is not valid")

        return self.failures


class PipeDockerImageSizeValidator(PipeMetadataValidator):

    def validate(self):
        if not self.context.remote_yml:
            return

        dockerhub_api_service = DockerHubAPIService()
        image_name = self.context.remote_yml['image']
        # Validate docker image size
        try:
            image_size = dockerhub_api_service.get_size(image_name)
        except Exception:
            client = docker.from_env()
            image = client.images.pull(image_name)
            client.close()
            image_size = int(image.attrs['Size'])

        if image_size > 1073741824:
            self.failures.criticals.append(f"Docker image '{image_name}' is larger than 1GB.")

        return self.failures


class PipeRemoteYamlValidator(PipeMetadataValidator):
    def __init__(self, context: RuleContext):
        super().__init__(context)
        self.REMOTE_METADATA = cerberus.Validator(REMOTE_METADATA_SCHEMA)

    def validate(self):
        infile = self.context.infile
        if not self.REMOTE_METADATA.validate(self.context.remote_yml):
            # TODO: remove when all pipes migrate to new metadata
            if self.context.remote_yml.get('icon') or \
               not isinstance(self.context.remote_yml.get('maintainer'), dict):
                # This means the vendor pipe is not migrated yet to new metadata structure
                self.failures.warnings.append(
                    f"{infile} repository not valid: pipe.yml not valid: {str(self.REMOTE_METADATA.errors)}. "
                    f"Notify maintainer")
                return

            self.failures.criticals.append(
                f"{infile} repository not valid: pipe.yml not valid: {str(self.REMOTE_METADATA.errors)}")

        return self.failures


class PipeFinalYamlValidator(PipeMetadataValidator):
    def __init__(self, context: RuleContext):
        super().__init__(context)
        self.FINAL_METADATA = cerberus.Validator(FINAL_METADATA_SCHEMA)

    def validate(self):
        infile = self.context.infile
        if not self.FINAL_METADATA.validate(self.context.final_yml):
            # TODO: remove when all pipes migrate to new metadata
            if self.context.final_yml.get('icon') or \
               not isinstance(self.context.final_yml.get('maintainer'), dict):
                # This means the vendor pipe is not migrated yet to new metadata structure
                self.failures.warnings.append(f"{infile} final metadata not valid: {str(self.FINAL_METADATA.errors)}. "
                                              f"Notify maintainer.")
                return

            self.failures.criticals.append(f"{infile} final metadata not valid: {str(self.FINAL_METADATA.errors)}")

        return self.failures


class PipeValidatorHandler:
    Pattern_T = Tuple[str, bool]

    def __init__(self, context: RuleContext, validators: List[PipeMetadataValidator]):
        self.context = context
        self.validators = validators
        self.failures = Failures(criticals=[], warnings=[], skipped=[])

    def validate(self):
        for ClassValidator in self.validators:
            result_failures = ClassValidator(self.context).validate()

            if result_failures is not None:
                self.failures.extend(result_failures)
                self.review_skipped()
        return self.failures

    def review_skipped(self):
        self.generate_skipped()
        self.failures.criticals = [item for item in self.failures.criticals if item not in self.failures.skipped]
        self.failures.warnings = [item for item in self.failures.warnings if item not in self.failures.skipped]

    def generate_skipped(self):
        self.failures.skipped = []
        if self.context.infile in WHITELIST_PIPES:
            for critical in self.failures.criticals:
                if PipeValidatorHandler.check_in_container(critical, WHITELIST_PIPES[self.context.infile]):
                    self.failures.skipped.append(critical)

            for warning in self.failures.warnings:
                if PipeValidatorHandler.check_in_container(warning, WHITELIST_PIPES[self.context.infile]):
                    self.failures.skipped.append(warning)

    @staticmethod
    def check_one(str_: str, pattern: Pattern_T) -> bool:
        """
        :param str_:
        :param pattern: (str, bool: is string a regex string)
        :return: bool
        """
        return bool(re.match(pattern[0], str_) if pattern[1] else str_ == pattern[0])

    @staticmethod
    def check_in_container(str_: str, container: List[Pattern_T]) -> bool:
        return any(PipeValidatorHandler.check_one(str_, item) for item in container)
